from django.conf.urls import url, include
from django.contrib import admin

from rest_framework import routers
from cv import views

from django.contrib.staticfiles.urls import staticfiles_urlpatterns

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)
router.register(r'album', views.AlbumViewSet)
router.register(r'train', views.PersonFacesViewSet)


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    # API
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^recognize/$', views.RecognizeView.as_view()),
    url(r'^detect/$', views.DetectView.as_view()),
]

urlpatterns += staticfiles_urlpatterns()
