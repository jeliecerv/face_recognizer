#!/usr/bin/env python
from models import Face
import os
import sys
import cv2
import numpy as np

import random
rand = random.Random()

PATH = './att_faces'


# added a names list(z)
def read_images(path, cascade, sz=None):
    """Reads the images in a given folder, resizes images on the fly if size is given.

    Args:
        path: Path to a folder with subfolders representing the subjects (persons).
        sz: A tuple with the size Resizes

    Returns:
        A list [X,y,z]

            X: The images, which is a Python list of numpy arrays.
            y: The corresponding labels (the unique number of the subject, person) in a Python list.
            z: A list of person-names, indexed by label
    """
    c = 0
    X, y, z = [], [], []
    for dirname, dirnames, filenames in os.walk(path):
        for subdirname in dirnames:
            subject_path = os.path.join(dirname, subdirname)
            for filename in os.listdir(subject_path):
                try:
                    im = cv2.imread(os.path.join(subject_path, filename), cv2.IMREAD_GRAYSCALE)
                    if (len(im) == 0):
                        continue  # not an image
                    # resize to given size (if given)
                    if (sz is not None):
                        im = cv2.resize(im, sz)

                    rects = cascade.detectMultiScale(im, scaleFactor=1.3, minNeighbors=4, minSize=(30, 30), flags=cv2.CASCADE_SCALE_IMAGE)

                    if len(rects) == 0:
                        os.remove(os.path.join(subject_path, filename))
                        continue  # not found faces

                    # If face is detected, append the face to images and the label to labels
                    for (x, y2, w, h) in rects:
                        # images.append(image[y: y + h, x: x + w])
                        X.append(np.asarray(im, dtype=np.uint8))
                        # X.append(np.asarray(im[y: y2 + h, x: x + w], dtype=np.uint8))
                        y.append(c)

                except IOError, (errno, strerror):
                    print "I/O error({0}): {1}".format(errno, strerror)
                except:
                    print "Unexpected error:", sys.exc_info()[0]
                    raise
            c = c + 1
            z.append(subdirname)
    return [X, y, z]


# reload the images & retrain the model.
# note, that lbp would give you the possibility of
# just updating the model with additonal data instead.
def retrain(imgpath, model, sz, cascade):
    # read in the image data. This must be a valid path!
    X, y, names = read_images(imgpath, cascade, sz)
    if len(X) == 0:
        print "image path empty", imgpath
        return [[], [], []]
    # Learn the model. Remember our function returns Python lists,
    # so we use np.asarray to turn them into NumPy lists to make
    # the OpenCV wrapper happy:
    # Also convert labels to 32bit integers. This is a workaround for 64bit machines,
    model.train(np.asarray(X), np.asarray(y, dtype=np.int32))
    return [X, y, names]


def face_detect(image_path):
    face_haarcascade = './haarcascade_frontalface_default.xml'
    cascade = cv2.CascadeClassifier(face_haarcascade)

    img = cv2.imread(image_path, cv2.IMREAD_COLOR)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    gray = cv2.equalizeHist(gray)
    rects = cascade.detectMultiScale(gray, scaleFactor=1.3, minNeighbors=4, minSize=(30, 30), flags=cv2.CASCADE_SCALE_IMAGE)

    face = Face()
    for x, y, w, h in rects:
        face.rects_face.append(
            {
                'x': x,
                'y': y,
                'w': w,
                'h': h,
            })

    return face


def face_rec(image_path, album):
    eye_haarcascade = './haarcascade_eye_tree_eyeglasses.xml'
    face_haarcascade = './haarcascade_frontalface_default.xml'

    # create the img folder, if nessecary
    imgdir = os.path.join(PATH, album)
    try:
        os.mkdir(imgdir)
    except:
        pass  # dir already existed

    # default face size, all faces in the db need to be the same.
    face_size = (100, 100)

    # load the cascadefile:
    cascade = cv2.CascadeClassifier(face_haarcascade)
    if (cascade.empty()):
        print "no cascade!"
        sys.exit()

    eye_cascade = cv2.CascadeClassifier(eye_haarcascade)

    # Create the model. We are going to use the default
    # parameters for this simple example, please read the documentation
    modelEigen = cv2.face.createEigenFaceRecognizer()
    modelFisher = cv2.face.createFisherFaceRecognizer()
    modelLBPH = cv2.face.createLBPHFaceRecognizer()

    # train it from faces in the imgdir:
    # images, labels, names = retrain(imgdir, modelEigen, face_size, cascade)
    # print "Eigen trained:", len(images), "images", len(names), "persons"
    # images, labels, names = retrain(imgdir, modelFisher, face_size, cascade)
    # print "Fisher trained:", len(images), "images", len(names), "persons"
    images, labels, names = retrain(imgdir, modelLBPH, face_size, cascade)
    print "LBPH trained:", len(images), "images", len(names), "persons"

    img = cv2.imread(image_path, cv2.IMREAD_COLOR)
    # gray = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)
    # img = image_path
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    gray = cv2.equalizeHist(gray)
    # try to detect a face in the img:
    rects = cascade.detectMultiScale(gray, scaleFactor=1.3, minNeighbors=4, minSize=(50, 50), flags=cv2.CASCADE_SCALE_IMAGE)
    print rects

    # roi will keep the cropped face image ( if there was one )
    roi = None
    name = "unknown"
    p_confidence = 0
    face = Face()
    try:
        for x, y, w, h in rects:
            face.rects_face.append(
                {
                    'x': x,
                    'y': y,
                    'w': w,
                    'h': h,
                })
            # crop & resize it
            roi = cv2.resize(gray[y:y + h, x:x + h], face_size)
            # give some visual feedback for the cascade detection
            cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0))

            # Eyes cascade
            eyes = eye_cascade.detectMultiScale(gray, scaleFactor=1.3, minNeighbors=4, minSize=(30, 30), flags=cv2.CASCADE_SCALE_IMAGE)
            for ex, ey, ew, eh in eyes:
                cv2.rectangle(img, (ex, ey), (ex + ew, ey + eh), (0, 0, 255))

            if len(images) > 0:
                # model.predict is going to return the predicted label and
                # the associated confidence:
                labels = []
                confidences = []
                [p_label, p_confidence] = modelLBPH.predict(np.asarray(roi))
                labels.append(p_label)
                confidences.append(p_confidence)
                # [p_label, p_confidence] = modelFisher.predict(np.asarray(roi))
                # labels.append(p_label)
                # confidences.append(p_confidence)
                # [p_label, p_confidence] = modelEigen.predict(np.asarray(roi))
                # labels.append(p_label)
                # confidences.append(p_confidence)
                # print labels, confidences
                print labels, confidences
                # [p_label, p_confidence] = get_result(labels, confidences)
                name = p_label = -1 if float(p_confidence) > 95 else p_label
                if p_label != -1:
                    name = names[p_label]
                else:
                    p_confidence = 0
                print("%s %.2f" % (name, p_confidence))
                cv2.putText(img, "%s %.2f" % (name, p_confidence), (x + 10, y + 20), cv2.FONT_HERSHEY_PLAIN, 1.3, (0, 200, 0))
                face.id = name
                face.confidence = p_confidence
    except Exception, e:
        pass

    return face
    # cv2.imwrite(os.path.join(imgdir, "result.jpg"), img)


def get_result(labels, confidences):
    labels_unique = list(set([x for x in labels if labels.count(x) > 1]))
    if len(labels_unique) > 0:
        index = labels.index(labels_unique[0])
        return labels_unique[0], confidences[index]

    return -1, 0


def preprocessing_images(path, images, cascade=None, sz=None):
    z = []

    if cascade is None:
        face_haarcascade = './haarcascade_frontalface_default.xml'
        cascade = cv2.CascadeClassifier(face_haarcascade)

    for filename in images:
        try:
            im = cv2.imread(os.path.join(filename), cv2.IMREAD_GRAYSCALE)
            if (len(im) == 0):
                continue  # not an image

            # resize to given size (if given)
            if (sz is not None):
                im = cv2.resize(im, sz)

            rects = cascade.detectMultiScale(im, scaleFactor=1.3, minNeighbors=4, minSize=(30, 30), flags=cv2.CASCADE_SCALE_IMAGE)

            # If face is detected, append the face to images and the label to labels
            if len(rects) == 0:
                os.remove(os.path.join(filename))
                continue  # not found faces

            count_face = 0
            for (x, y, w, h) in rects:
                file_name = os.path.basename(filename)
                filename_face = str(count_face) + file_name
                cv2.imwrite(os.path.join(path, filename_face), im[y: y + h, x: x + w])

                # Mirror image
                im_mirror = cv2.flip(im[y: y + h, x: x + w], 1)
                filename_face_mirror = 'm' + str(count_face) + file_name
                cv2.imwrite(os.path.join(path, filename_face_mirror), im_mirror)

                # Rotate image
                rows, cols = im[y: y + h, x: x + w].shape
                M = cv2.getRotationMatrix2D((cols / 2, rows / 2), -15, 1)
                dst = cv2.warpAffine(im[y: y + h, x: x + w], M, (cols, rows))
                filename_face_rotate = 'r' + str(count_face) + file_name
                cv2.imwrite(os.path.join(path, filename_face_rotate), dst)

                z.append(filename_face)
                if count_face == 0:
                    os.remove(os.path.join(filename))
                count_face += 1
        except IOError, (errno, strerror):
            print "I/O error({0}): {1}".format(errno, strerror)
        except:
            print "Unexpected error:", sys.exc_info()[0]
            raise

    return z
