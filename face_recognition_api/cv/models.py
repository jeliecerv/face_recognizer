from __future__ import unicode_literals

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
import os
import urllib

PATH = './att_faces'


class Album(models.Model):
    name = models.CharField('Name', max_length=256)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('name',)


class PersonFaces(models.Model):
    name = models.CharField('Name', max_length=256)
    urls = models.TextField('URLs')
    album = models.ForeignKey(Album, related_name='person_faces')

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('name',)


class Face(object):

    def __init__(self):
        self.id = -1
        self.confidence = 0
        self.name = "unknown"
        self.rects_face = []
        self.image = ""


@receiver(post_save, sender=Album, dispatch_uid="create_dir")
def create_dir(sender, instance, **kwargs):
    directory = os.path.join(PATH, str(instance.pk))
    if not os.path.isdir(directory):
        os.mkdir(directory)
