from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from facerec_online import *
from models import Album, PersonFaces
from serializers import UserSerializer, GroupSerializer, AlbumSerializer, PersonFacesSerializer
from django.contrib.staticfiles.templatetags.staticfiles import static
import os
import logging
import urllib
import urllib2
import uuid


PATH = './att_faces'
logger = logging.getLogger(__name__)


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class AlbumViewSet(viewsets.ModelViewSet):
    queryset = Album.objects.all()
    serializer_class = AlbumSerializer


class PersonFacesViewSet(viewsets.ModelViewSet):
    queryset = PersonFaces.objects.all()
    serializer_class = PersonFacesSerializer
    parser_classes = (FormParser, MultiPartParser,)

    def create(self, request):
        print 'train create'
        try:
            album = Album.objects.get(pk=request.data['album'])
            person_faces = PersonFaces.objects.create(name=request.data['name'], urls=request.data['urls'], album=album)

            self.get_images(person_faces, request.data['urls'].split(','))

            serializer = PersonFacesSerializer(person_faces, context={'request': request})

            return Response(serializer.data, status=status.HTTP_200_OK)
        except Exception, e:
            print str(e)
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, *args, **kwargs):
        try:
            print 'update train'
            album = Album.objects.get(pk=request.data['album'])
            person_faces = PersonFaces.objects.get(pk=kwargs['pk'])
            person_faces.name = request.data['name']
            person_faces.urls = person_faces.urls + ',' + request.data['urls']
            person_faces.album = album
            person_faces.save()

            self.get_images(person_faces, request.data['urls'].split(','))

            serializer = PersonFacesSerializer(person_faces, context={'request': request})

            return Response(serializer.data, status=status.HTTP_200_OK)
        except Exception, e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)

    def get_images(self, instance, images):
        directory = os.path.join(PATH, str(instance.album.id))
        if not os.path.isdir(directory):
            os.mkdir(directory)

        directory_face = os.path.join(directory, str(instance.id))
        if not os.path.isdir(directory_face):
            os.mkdir(directory_face)

        paths = images
        result_image = []
        count = len([name for name in os.listdir(directory_face) if os.path.isfile(os.path.join(directory_face, name))])
        for path in paths:
            page = urllib2.urlopen(path)
            pageHeaders = page.headers
            contentType = pageHeaders.getheader('content-type')
            image_name = os.path.join(directory_face, str(count) + "." + contentType.split('/')[1])
            urllib.urlretrieve(path, image_name)
            result_image.append(image_name)
            count += 1

        # face_size = (100, 100)
        names = preprocessing_images(directory_face, result_image)
        print names


class DetectView(APIView):
    parser_classes = (FormParser, MultiPartParser,)

    def post(self, request):
        unique_filename = str(uuid.uuid4()) + '.png'
        filename = os.path.join(PATH, 'temp', unique_filename)
        try:
            if 'file' in request.data:
                file_obj = request.data['file']

                destination = open(filename, 'wb+')

                for chunk in file_obj.chunks():
                    destination.write(chunk)
                    destination.close()
            elif 'url' in request.data:
                urllib.urlretrieve(request.data['url'], filename)

            face = face_detect(filename)

            return Response(face.__dict__, status=status.HTTP_200_OK)
        except Exception, e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        finally:
            os.remove(filename)


class RecognizeView(APIView):
    parser_classes = (FormParser, MultiPartParser,)

    def post(self, request):
        unique_filename = str(uuid.uuid4()) + '.png'
        filename = os.path.join(PATH, 'temp', unique_filename)
        try:
            if 'file' in request.data:
                file_obj = request.data['file']

                destination = open(filename, 'wb+')

                for chunk in file_obj.chunks():
                    destination.write(chunk)
                    destination.close()
            elif 'url' in request.data:
                urllib.urlretrieve(request.data['url'], filename)

            face = face_rec(filename, request.data['album'])

            name = 'unknown'
            if face.id != -1 and face.id.isnumeric():
                person_face = PersonFaces.objects.get(pk=face.id)
                name = person_face.name
                files_image = sorted(os.listdir(os.path.join(PATH, request.data['album'], face.id)))
                if(len(files_image) > 0):
                    face.image = 'http://' + request.META['HTTP_HOST'] + static(os.path.join(request.data['album'], face.id, files_image[0]))
            else:
                face.id = -1
                face.confidence = 0

            face.name = name

            return Response(face.__dict__, status=status.HTTP_200_OK)
        except Exception, e:
            return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        finally:
            os.remove(filename)
