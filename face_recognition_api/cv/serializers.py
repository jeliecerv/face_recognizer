from django.contrib.auth.models import User, Group
from rest_framework import serializers
from models import Album, PersonFaces


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')


class PersonFacesSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = PersonFaces
        fields = ('id', 'name', 'urls', 'album')


class AlbumSerializer(serializers.HyperlinkedModelSerializer):
    person_faces = PersonFacesSerializer(read_only=True, many=True)

    class Meta:
        model = Album
        fields = ('id', 'name', 'person_faces')
