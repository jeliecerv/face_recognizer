#!/usr/bin/env python
import os
import sys
import cv2
import numpy as np

import random
rand = random.Random()


# added a names list(z)
def read_images(path, cascade, sz=None):
    """Reads the images in a given folder, resizes images on the fly if size is given.

    Args:
        path: Path to a folder with subfolders representing the subjects (persons).
        sz: A tuple with the size Resizes

    Returns:
        A list [X,y,z]

            X: The images, which is a Python list of numpy arrays.
            y: The corresponding labels (the unique number of the subject, person) in a Python list.
            z: A list of person-names, indexed by label
    """
    c = 0
    X, l, z = [], [], []
    for dirname, dirnames, filenames in os.walk(path):
        for subdirname in dirnames:
            subject_path = os.path.join(dirname, subdirname)
            for filename in os.listdir(subject_path):
                try:
                    im = cv2.imread(os.path.join(subject_path, filename), cv2.IMREAD_GRAYSCALE)
                    if (len(im) == 0):
                        continue  # not an image

                    rects = cascade.detectMultiScale(im, scaleFactor=1.3, minNeighbors=4, minSize=(30, 30), flags=cv2.CASCADE_SCALE_IMAGE)

                    if len(rects) == 0:
                        os.remove(os.path.join(subject_path, filename))
                        continue  # not found faces

                    # If face is detected, append the face to images and the label to labels
                    for (x, y, w, h) in rects:
                        # resize to given size (if given)
                        if (sz is not None):
                            im = cv2.resize(im[y: y + h, x: x + w], sz)

                        # images.append(image[y: y + h, x: x + w])
                        X.append(np.asarray(im, dtype=np.uint8))
                        l.append(c)

                        # cv2.rectangle(im, (x, y2), (x + w, y2 + h), (0, 255, 0), 2)
                        # cv2.imshow("Adding faces to traning set...", im)
                        # cv2.waitKey(200)
                        print(filename)

                except IOError(errno, strerror):
                    print("I/O error({0}): {1}".format(errno, strerror))
                except:
                    print("Unexpected error:", sys.exc_info()[0])
                    raise
            c = c + 1
            z.append(subdirname)
    return [X, l, z]


# reload the images & retrain the model.
# note, that lbp would give you the possibility of
# just updating the model with additonal data instead.
def retrain(imgpath, model, sz, cascade):
    # read in the image data. This must be a valid path!
    X, y, names = read_images(imgpath, cascade, sz)
    if len(X) == 0:
        print("image path empty", imgpath)
        return [[], [], []]
    # Learn the model. Remember our function returns Python lists,
    # so we use np.asarray to turn them into NumPy lists to make
    # the OpenCV wrapper happy:
    # Also convert labels to 32bit integers. This is a workaround for 64bit machines,
    model.train(np.asarray(X), np.asarray(y, dtype=np.int32))
    return [X, y, names]


if __name__ == "__main__":
    eye_haarcascade = 'haarcascade_eye.xml'
    smile_haarcascade = 'haarcascade_smile.xml'
    face_haarcascade = 'haarcascade_frontalface_default.xml'
    # You'll need the path to your image folder, also we need to find
    # a haar/lbpcascade for detecting faces, e.g. opencv\data\haarcascades\haarcascade_frontalface_alt2.xml
    # if len(sys.argv) < 3:
    #     print "USAGE: facerec_online.py </path/to/images>"
    #     sys.exit()

    print("  press 'esc' to quit")
    print("  press 'a' to append a new face to the database")
    print("      (you'll be prompted for a name on the console)")
    print("  press 't' to retrain the model (if you appended faces there)")

    # create the img folder, if nessecary
    imgdir = "./att_faces"
    try:
        os.mkdir(imgdir)
    except:
        pass  # dir already existed

    # default face size, all faces in the db need to be the same.
    face_size = (100, 100)

    # open the webcam
    cam = cv2.VideoCapture(0)
    if (not cam.isOpened()):
        print("no cam!")
        sys.exit()
    print("cam: ok.")

    # load the cascadefile:
    cascade = cv2.CascadeClassifier(face_haarcascade)
    if (cascade.empty()):
        print("no cascade!")
        sys.exit()

    eye_cascade = cv2.CascadeClassifier(eye_haarcascade)
    smile_cascade = cv2.CascadeClassifier(smile_haarcascade)

    # Create the model. We are going to use the default
    # parameters for this simple example, please read the documentation
    # train it from faces in the imgdir:

    modelLBPH = cv2.face.createLBPHFaceRecognizer(threshold=90.0)
    if(os.path.exists("save_lbph.yaml")):
        modelLBPH.load("save_lbph.yaml")
        print("trained LBPH Load:")
    else:
        images, labels, names = retrain(imgdir, modelLBPH, face_size, cascade)
        modelLBPH.save("save_lbph.yaml")
        print("trained LBPH:", len(images), "images", len(names), "persons")

    modelEigen = cv2.face.createEigenFaceRecognizer(threshold=5000.0)
    if(os.path.exists("save_eigen.yaml")):
        modelEigen.load("save_eigen.yaml")
        print("trained Eigen Load:")
    else:
        images, labels, names = retrain(imgdir, modelEigen, face_size, cascade)
        modelEigen.save("save_eigen.yaml")
        print("trained Eigen:", len(images), "images", len(names), "persons")

    modelFisher = cv2.face.createFisherFaceRecognizer(threshold=5000.0)
    if(os.path.exists("save_fisher.yaml")):
        modelFisher.load("save_eigen.yaml")
        print("trained Eigen Load:")
    else:
        images, labels, names = retrain(imgdir, modelFisher, face_size, cascade)
        modelFisher.save("save_fisher.yaml")
        print("trained Fisher:", len(images), "images", len(names), "persons")

    cv2.destroyAllWindows()

    predicted_label = -1
    confidence = 0.0
    # collector = cv2.face.createPredictCollector(predicted_label, confidence)
    while True:
        ret, img = cam.read()
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        gray = cv2.equalizeHist(gray)
        # try to detect a face in the img:
        rects = cascade.detectMultiScale(gray, scaleFactor=1.3, minNeighbors=8, minSize=(50, 50), flags=cv2.CASCADE_SCALE_IMAGE)

        # roi will keep the cropped face image ( if there was one )
        roi = None
        for x, y, w, h in rects:
            # crop & resize it
            roi = cv2.resize(gray[y:y + h, x:x + h], face_size)
            # give some visual feedback for the cascade detection
            cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0))

            # Eyes cascade
            eyes = eye_cascade.detectMultiScale(gray, scaleFactor=1.3, minNeighbors=4, minSize=(30, 30), flags=cv2.CASCADE_SCALE_IMAGE)
            for ex, ey, ew, eh in eyes:
                cv2.rectangle(img, (ex, ey), (ex + ew, ey + eh), (0, 0, 255))

            # mount cascade
            # smile = smile_cascade.detectMultiScale(gray[y:y + h, x:x + h], scaleFactor=1.3, minNeighbors=4, minSize=(30, 30), flags=cv2.CASCADE_SCALE_IMAGE)
            # for sx, sy, sw, sh in smile:
            #    cv2.rectangle(img, (sx + x, sy + y), ((sx + sw) + x, (sy + sh) + y), (0, 255, 0))

            # if len(images) > 0:
                # model.predict is going to return the predicted label and
                # the associated confidence:
                p_label = modelEigen.predict(np.asarray(roi))
                print("modelEigen", p_label)

                p_label = modelFisher.predict(np.asarray(roi))
                print("modelFisher", p_label)

                p_label = modelLBPH.predict(np.asarray(roi))
                print("modelLBPH", p_label)

                # name = "unknown"
                # p_confidence = 0
                # if p_label != -1:
                #     print(modelLBPH.getLabelInfo(p_label), 'Label info')
                #     # name = names[p_label]
                #     name = p_label
                # cv2.putText(img, "%s %.2f" % (name, p_confidence), (x + 10, y + 20), cv2.FONT_HERSHEY_PLAIN, 1.3, (0, 200, 0))
            break  # use only 1st detected

        cv2.imshow('facedetect', img)

        k = cv2.waitKey(5) & 0xFF

        # bailout on 'esc'
        if k == 27:
            break

        # 'a' pressed, add person to the database
        if (k == 97) and (roi is not None):
            print("please input the name: ")
            name = sys.stdin.readline().strip('\r').strip('\n')
            # make a folder for that person:
            dirname = os.path.join(imgdir, name)
            try:
                os.mkdir(dirname)
            except:
                pass  # dir already existed
            # save image
            path = os.path.join(dirname, "%d.png" % (rand.uniform(0, 10000)))
            print("added:", path)
            cv2.imwrite(path, roi)

        # if enough new data was collected, retrain the model
        if (k == 116):  # 't' pressed
            images, labels, names = retrain(imgdir, modelLBPH, face_size, cascade)
            print("trained:", len(images), "images", len(names), "persons")
