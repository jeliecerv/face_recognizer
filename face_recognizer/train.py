# train.py
import cv2
import sys
import os
from croper import CropFace
from PIL import Image

size = 4
fn_haar = 'haarcascade_frontalface_default.xml'
fn_dir = 'att_faces'
fn_name = sys.argv[1]
path = os.path.join(fn_dir, fn_name)
if not os.path.isdir(path):
    os.mkdir(path)
(im_width, im_height) = (100, 100)
haar_cascade = cv2.CascadeClassifier(fn_haar)
webcam = cv2.VideoCapture(0)


def reorient_face(img_cv, count_name):
    eye_haarcascade1 = './haarcascade_lefteye_2splits.xml'
    eye_haarcascade2 = './haarcascade_eye_tree_eyeglasses.xml'
    eye_cascade1 = cv2.CascadeClassifier(eye_haarcascade1)
    eye_cascade2 = cv2.CascadeClassifier(eye_haarcascade2)

    im_org1 = Image.fromarray(img_cv)  # Image.open(filename)
    im_org2 = img_cv  # cv2.imread(filename)
    gray = img_cv  # cv2.cvtColor(im_org2, cv2.COLOR_BGR2GRAY)
    gray = cv2.equalizeHist(gray)

    eye1 = eye_cascade_processor(gray, eye_cascade1)
    eye2 = eye_cascade_processor(gray, eye_cascade2)

    count = 0
    if len(eye1) > 1 or len(eye2) > 1:
        eyes = eye1 if len(eye1) > 1 else eye2
        if len(eyes) > 1:
            for ex, ey, ew, eh in eyes:
                if count == 0:
                    eye_right_x = ex
                    eye_right_y = ey
                    count += 1
                else:
                    eye_left_x = ex
                    eye_left_y = ey
        else:
            eye_right_x = 0
            eye_right_y = 0
            eye_left_x = 0
            eye_left_y = 0

    if count == 1:
        crop_face = CropFace(im_org1, eye_left=(eye_left_x, eye_left_y),
                                        eye_right=(eye_right_x, eye_right_y),
                                        offset_pct=(0.3, 0.3), dest_sz=(92, 112))

        filename_face_reorient = 'reo' + str(count_name) + '.png'
        crop_face.save(os.path.join(path, filename_face_reorient))


def eye_cascade_processor(gray, eye_cascade):
    eyes = eye_cascade.detectMultiScale(gray,
                                        scaleFactor=1.3,
                                        minNeighbors=4,
                                        minSize=(30, 30),
                                        flags=cv2.CASCADE_SCALE_IMAGE
                                        )
    return eyes

# The program loops until it has 20 images of the face.
count = 0
while count < 20:
    (rval, im) = webcam.read()
    im = cv2.flip(im, 1, 0)

    gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
    mini = cv2.resize(gray, (gray.shape[1] / size, gray.shape[0] / size))
    faces = haar_cascade.detectMultiScale(mini, scaleFactor=1.3, minNeighbors=4, minSize=(30, 30), flags=cv2.CASCADE_SCALE_IMAGE)
    faces = sorted(faces, key=lambda x: x[3])

    if faces:
        face_i = faces[0]
        (x, y, w, h) = [v * size for v in face_i]
        face = gray[y:y + h, x:x + w]

        reorient_face(face, count)

        face_resize = cv2.resize(face, (im_width, im_height))
        # pin = sorted([int(n[:n.find('.')]) for n in os.listdir(path)
        #        if n[0] != '.'] + [0])[-1] + 1
        cv2.imwrite('%s/%s.png' % (path, count), face_resize)
        cv2.rectangle(im, (x, y), (x + w, y + h), (0, 255, 0), 3)
        cv2.putText(im, fn_name, (x - 10, y - 10), cv2.FONT_HERSHEY_PLAIN,
            1, (0, 255, 0))

        # Mirror image
        im_mirror = cv2.flip(face_resize, 1)
        filename_face_mirror = 'm' + str(count) + '.png'
        cv2.imwrite(os.path.join(path, filename_face_mirror), im_mirror)

        # Rotate image
        rows, cols = face_resize.shape
        M = cv2.getRotationMatrix2D((cols / 2, rows / 2), 15, 1)
        dst = cv2.warpAffine(face_resize, M, (cols, rows))
        filename_face_rotate = 'r' + str(count) + '.png'
        cv2.imwrite(os.path.join(path, filename_face_rotate), dst)

        M = cv2.getRotationMatrix2D((cols / 2, rows / 2), -15, 1)
        dst = cv2.warpAffine(face_resize, M, (cols, rows))
        filename_face_rotate_i = 'ri' + str(count) + '.png'
        cv2.imwrite(os.path.join(path, filename_face_rotate_i), dst)

        count += 1

    cv2.imshow('OpenCV', im)
    key = cv2.waitKey(10)
    if key == 27:
        break
